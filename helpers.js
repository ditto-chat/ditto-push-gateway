const apn = require('apn');
const base64 = require('base-64');
const admin = require("firebase-admin");

const serviceAccount = require('./serviceAccountKey.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://ditto-chat-mobile.firebaseio.com"
});

const apnOptions = {
  token: {
    key: "./keyfile.p8",
    keyId: '3V33734GV6',
    teamId: '482A2E7WX8'
  },
  production: false
};

const apnProvider = new apn.Provider(apnOptions);

function handleIos(notif, device) {
  const note = new apn.Notification({
    mutableContent: 1,
    contentAvailable: 1
  });

  note.badge = notif.counts.unread;
  if (notif.room_name) {
    note.title = notif.room_name
  }
  note.body = notif.content && notif.sender_display_name ? `${notif.sender_display_name}: ${notif.content?.body}` : 'You received a message';
  note.payload = {'messageFrom': notif.sender_display_name, roomId: notif.room_id};
  note.topic = device.app_id;

  const token = base64.decode(device.pushkey)
  return apnProvider.send(note, token)
}

function handleAndroid(notif, device) {
  const token = base64.decode(device.pushkey)
  // Send a message to devices subscribed to the provided topic.
  return admin.messaging().sendToDevice(token, {
    notification: {
      ...(notif.room_name ? { title: 'Message from node' } : {}),
      body: notif.content && notif.sender_display_name ? `${notif.sender_display_name}: ${notif.content?.body}` : 'You received a message',
      badge: `${notif.counts.unread}`,
    },
  })
}

module.exports = {handleIos, handleAndroid}