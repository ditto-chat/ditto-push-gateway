'use strict';

const express = require('express');
const bodyParser = require('body-parser')
const multer = require('multer') // v1.0.5
const upload = multer(); // for parsing multipart/form-data
const app = express();

const apps = require('./apps')
const {handleIos, handleAndroid} = require('./helpers')

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

// Constants
const PORT = 5000;
const HOST = '0.0.0.0';

// App
app.get('/', (req, res) => {
  res.status(405).send('Method Not Allowed')
});

app.post('/', (req, res) => {
  res.status(400).send('Post request should be sent to /_matrix/push/v1/notify')
});

app.post('/_matrix/push/v1/notify', upload.array(), (req, res) => {
  if (!req) {
    res.status(400).send('No request data sent.')
    return;
  }
  const notif = req.body.notification
  const notifPromises = []

  if (!notif.room_id) {
    console.log('This event has no room ID, quitting early.')
    res.status(200).send('No room ID.')
    return;
  }

  notif.devices.forEach(device => {
    const appId = device.app_id

    if (apps.apns.includes(appId)) {
       // handle iOS
      console.log('Request received at ', new Date().toString(), '\n', notif)
      notifPromises.push(handleIos(notif, device))
    } else if (apps.fcm.includes(appId)) {
      // handle android
      console.log('Request received at ', new Date().toString(), '\n', notif)
      notifPromises.push(handleAndroid(notif, device))
    }
  })

  Promise.all(notifPromises).then(result => {
    const rejected = []
    result.forEach(notifResult => {
      if (notifResult.failed && notifResult.failed.length > 0) {
        rejected.push(...notifResult.failed.map(i => i.device))
      } else if (notifResult.failureCount > 0) {
        rejected.push(...notifResult.results.map(i => i.messageId))
      }
    })
    console.log('Finished sending notifs for event, rejected: ', rejected)
    res.status(200).send({rejected})
  }).catch(e => {
    res.status(500).send(e.toString())
  })

})

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}, logs at about:inspect in Chrome`);